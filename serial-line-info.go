package main

import (
	"fmt"
	"log"
	"strings"
	"os"
	"github.com/tarm/serial"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func main() {
	Consoles := []string{
		"/dev/ttyUSB0",
		"/dev/ttyUSB1",
		"/dev/ttyUSB2",
		"/dev/ttyUSB3",
		"/dev/ttyUSB4",
		"/dev/ttyUSB5",
		"/dev/ttyUSB6",
		"/dev/ttyUSB7",
		"/dev/ttyUSB8",
		"/dev/ttyUSB9",
		"/dev/ttyUSB10",
		"/dev/ttyUSB11",
		"/dev/ttyUSB12",
		"/dev/ttyUSB13",
		"/dev/ttyUSB14",
		"/dev/ttyUSB15",
		"/dev/ttyUSB16",
		"/dev/ttyUSB17",
		"/dev/ttyUSB18",
		"/dev/ttyUSB19",
		"/dev/ttyUSB20",
		}

	for _, Console := range Consoles {

		if fileExists(Console) {
		fmt.Println("###################")
		fmt.Println(Console ," here... trying to collect info...  ")



		config := &serial.Config{
			Name:        Console,
			Baud:        115200,
			ReadTimeout: 10,
			Size:        8,
		}



		stream, err := serial.OpenPort(config)
		if err != nil {
			log.Fatal(err)
		}

		buf := make([]byte, 2048)

		n, err := stream.Write([]byte("\r \r"))
		if err != nil {
			log.Fatal(err)
			fmt.Print("oo", n)
		}
		for {
			n, err := stream.Read(buf)
			if err != nil {
			break
			}
			s := string(buf[:n])
			if strings.Contains(s, "netdc") {
				fmt.Println(Console,s)
			}
			fmt.Print(s)
		}
	} else {
		fmt.Println(Console ,"not there;  ")

		}
	}
	fmt.Println ("...")
	fmt.Println( "thanks, wel done!")
}

